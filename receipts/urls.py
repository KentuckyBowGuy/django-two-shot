from django.urls import path
from .views import (
    receipts_list,
    create_receipt,
    expensecategory_list,
    account_list,
    create_category,
    create_account,
)

urlpatterns = [
    path("", receipts_list, name="home"),
    path("receipts/", receipts_list, name="receipt_list"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
    path("categories/", expensecategory_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
]
